
$(document).ready(function() {

    $('.speaker').click(function(e){
        $('.vegas-video').prop('muted', !$('.vegas-video').prop('muted'));
        e.preventDefault();
        $(this).toggleClass('mute');
    })

    $(window).load(function() {
        $('.homepage-menu').addClass('active');
        var width = $(window).width();
        if (width < 768){
            $('.cs-style-4 li').click(function(){
                $("figcaption").hide(100);
                $(this).find("figcaption").show(100);
            })
        }
    });

    //Animation effect
    snowStorm = new SnowStorm();

    /* Home Slideshow Vegas
    -----------------------------------------------*/
    $(function() {
        $('body').vegas({
            slides: [
                {
                    src: 'assets/img/picture/video-bg.jpg',
                    video: {
                        src: [
                            'https://view.vzaar.com/14507647/video'
                        ],
                        loop: false,
                        mute: true
                    },
                    delay : 30000
                },
                { src: 'assets/img/picture/banner-1.png' },
                { src: 'assets/img/picture/banner-2.jpg' },
                { src: 'assets/img/picture/banner-3.png' }

            ],
            timer: false,
            delay: 6000,
            transition: [ 'zoomIn', ],
            animation: ['kenburns']
        });
    });
    $('#banner_previous').on('click', function () {
        $('body').vegas('previous');
    });
    $('#banner_next').on('click', function () {
        $('body').vegas('next');
    });
});
