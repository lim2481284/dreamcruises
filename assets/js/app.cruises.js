$(document).ready(function(){

    //Menu item function
    $(window).load(function() {
        $('.cruises-menu').addClass('active');
    });

    var width = $(window).width();
     if (width > 1200){
       var lang = localStorage.getItem("language");
       if(lang =="en_GB")
       {
           $('.cruises-tab li').css('width','20%');
           $('.cruises-tab li').css('margin','0');
           $('.nav-pills>li>a').css('height','60px');
       }
     }


    $('#imageGallery').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:9,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left'
    });

    $('.zoom').magnify({
        magnifiedWidth:1500,
        magnifiedHeight:1500
    });
})
