var slider;

var room_name=["皇宫豪华套房","皇宫套房","豪华露台客房","露台客房","海景客房","内侧客房"];
var room_name_en=["Palace Deluxe Suite","Palace Suite","Balcony Deluxe Stateroom","Balcony Stateroom","Oceanview Stateroom","Interior Stateroom"];
var room = [
    "宽敞舒适的空间，融合雍雅时尚的设施，让您尽享奢适，倍受尊宠；尽享休闲无忧的海上假期。",
    "位于13、15、16及17楼，坐拥高层视野，供您饱览海天一色的无限风光。 享一席私属写意天地，于雅致露台放眼海阔天空；或在舒适宽敞的室内空间，品味皇宫套房的别致雍雅。",
    "豪华专属露台让您随时沐浴阳光，感受海风拂面的惬意与舒适，配合奢华格调的客房设计及高级卫浴设备，细节之间，尽显非凡气派。",
    "私人露台供您饱览潮起汐落的迷人景致，恬静舒适的室内空间打造宾至如归的温馨体验。 精选欧洲进口高级寝具，以丝丝细缕妆点您的美梦。",
    "明亮的观景窗让壮阔的海景成为客房的天然壁画，同时将自然光线引入室内，编织丰富灵动的空间意趣。 搭配高雅时尚的室内设计，构筑悠闲写意的温馨氛围。",
    "静谧的内侧客房，以典雅简约设计带来放松感受。 温和舒适的灯光设计，为您点亮一份不与人扰的独享时光，令您一扫旅途中的疲惫，尽享无忧宁谧。"
];
var room_en = [
    "Featuring tasteful, contemporary furnishings, the spacious Palace Deluxe Suite offers an extraordinary standard of luxury. With butler service, gourmet coffee and tea, and a private balcony with binoculars, you might find it difficult to leave at all.",
    "From its perch on decks 13, 15, 16 and 17, the Palace Suite provides an uninterrupted view of the sky, sea, and horizon. Breathe it all in from the privacy of your elegant balcony, or take respite within the comfort of your spacious luxury suite.",
    "You can enjoy the sun and moon whenever it strikes your fancy, from your private balcony. And upon your return to the stateroom, everywhere you look, you’ll see thoughtfully selected  amenities and furniture in the homely, impeccably designed interior. ",
    "Experience the ebb and flow of the tides from the quiet comfort of your private Balcony Stateroom. A dream to relax in, whether on the plush bedding or sunny balcony, this simple stateroom provides a welcome respite from adventures at sea.",
    "With the sunlight glistening off the ocean and filtering through the finely appointed windows, the Oceanview Stateroom is warm and comforting. Relax and enjoy the view in this cosy abode. Or close the curtain and enjoy a nap on the sumptuous bedding. ",
    "Unwind in the peace and quiet of the Interior Stateroom, positioned for ultimate calm and serenity. You’ll be at ease in these quarters; well designed for space, with every comfort and convenience of home, plus a touch of Dream Cruise grandeur."
];
var ori_price = ['12999','12222','8799','8488','7999','7099'];
var promo_price = ['10488','9699','7222','6888','6333','5699'];
var size = ['41','37','22','20','16','16'];
var level =['位于9/10/11/12/13/15层','位于13/15/16/17层','位于8/9/10/11/12/13/15层','位于8/9/10/11/12/13/15层','位于5/9/10/11/12/13层 ','位于5/8/9/10/11/12/13/15层'];
var level_en =['Located on the 9/10/11/12/13/15 floor','Located on the 13/15/16/17 floor','Located on the 8/9/10/11/12/13/15 floor','Located on the 8/9/10/11/12/13/15 floor','Located on the 5/9/10/11/12/13 floor ','Located on the 5/8/9/10/11/12/13/15 floor'];
var gallery =[
    ['room-1-1.jpg','room-1-2.jpg','room-1-3.jpg','room-1-4.jpg','room-1-5.jpg','room-1-6.jpg'],
    ['room-2-1.jpg','room-2-2.jpg'],
    ['room-3-1.jpg'],
    ['room-4-1.jpg','room-2-2.jpg'],
    ['room-5-1.jpg','room-5-2.jpg'],
    ['room-6-1.jpg']
];
var equipment_list = [
    ["独立配备一张双人床（1.8 x 2.0米），欧洲进口高级床上用品","专属露台","起居及用餐空间*","24小时尊尚礼宾服务","平板电视(49或32寸)","独立卫浴附设浴缸丶独立淋浴间丶卫生间","可选择连通房或比邻房"],
    ["一张双人床及一张双人沙发床，欧洲进口高级床上用品","专属露台","24 小时尊尚礼宾服务","一台49寸平板电视","独立卫浴附设浴缸或淋浴（关爱客房只设淋浴）","可选择连通房 "],
    ["一张双人床，欧洲进口高级床上用品","提供单人／双人沙发床或壁柜床","专属露台","一台32寸平板电视","可选择连通房"],
    ["一张双人床，欧洲进口高级床上用品","提供单人／双人沙发床或壁柜床","专属露台","一台平板电视","可选择连通房或比邻房"],
    ["两张单人床，精选床上用品","提供单人／双人沙发床或壁柜床","一台32寸平板电视"],
    ["床及沙发床，优质床上用品","提供壁柜床，以容纳第3位旅客","一台平板电视","可选择连通房"]
];
var equipment_list_en = [
    ["Separate bedroom with 1 Queen-size bed (1800 x 2000mm) with sumptuous bedding","Private balcony","Contemporary sitting and dining area *","24-hour dedicated Butler Concierge Service","Flat-panel television in 49\" or 32\"","Private bathroom with full bathtub, separate shower/toilet","Inter-connecting and adjoining staterooms available"],
    ["Queen-size bed with sumptuous bedding together with sleeper double sofa bed","Private balcony.","24-hour dedicated Butler Concierge Service","One flat-panel television in 49\" available","Private bathroom with full bathtub / shower combination (wheelchair-accessible stateroom with shower only)","Inter-connecting rooms available. "],
    ["Queen-size bed with sumptuous bedding together with sleeper single / double sofa bed (optional)","Ceiling Pullman bed (optional)","Private balcony","One flat-panel television in 32\" available","Inter-connecting rooms available "],
    ["Queen-size bed with sumptuous bedding together with sleeper single sofa bed (optional).","Celling pullman bed (optional)","Private balcony.","One flat-panel television available.","Inter-connecting and adjoining staterooms available"],
    ["Two single beds with sumptuous bedding together with sleeper single / double sofa bed (optional)","Ceiling Pullman bed (optional)","One flat-panel television in 32\" available"],
    ["Beds and sofabed with fine bedding","Celling Pullman bed (optional) available for up to third berth","One flat-panel television available.","Inter-connecting rooms available. "]
];
var room_description = [
    "* 此房型共有2种稍为不同的间隔（独立客厅及非独立客厅），房间编配须按实际情况，由星梦邮轮全权决定。",
    "* 此房型共有2种稍为不同的间隔（独立客厅及非独立客厅），房间编配须按实际情况，由星梦邮轮全权决定。",
    "",
    "",
    "",
    "图片为二人房布局图，另有三人房及四人房可供选择。"
];
var room_description_en = [
    "* This stateroom category comes in two slightly different variations, one with a separate living room and the other without. Stateroom assignment is subject to the sole discretion of Dream Cruises.",
    "* This stateroom category comes in two slightly different variations, one with a separate living room and the other without. Stateroom assignment is subject to the sole discretion of Dream Cruises. ",
    "",
    "",
    "",
    "The image is for a stateroom configuration for 2 persons – other stateroom configurations for 3 persons and 4 persons are also available."
];

function updateRoomDetail(lang){
    if(lang==1)
    {
        $('.room-select').val('1');
        $('.room-select').change();
    }
    var roomID = $(".room-select").val();
    var pic = 'assets/img/picture/room-'+roomID+'.jpg';
    var g = gallery[roomID-1];
    var title =$($( ".room-select option:selected" )[0]).text();
    var lang = localStorage.getItem("language");
    if(lang =="en_GB")
    {
        var room_lang = room_en;
        var level_lang = level_en;
        var equip_lang = equipment_list_en[roomID-1];
        var room_desc_lang = room_description_en;
        var room_title_lang = room_name_en;
    }
    else
    {
        var room_lang = room;
        var level_lang = level;
        var equip_lang = equipment_list[roomID-1];
        var room_desc_lang = room_description;
        var room_title_lang = room_name;
    }

    //Update room detail
    $('.banner-img').attr('src',pic);
    $('.room-title').html(room_title_lang[roomID-1]);
    $('.room-description-content').html(room_lang[roomID-1]);
    $('.original-price').html(ori_price[roomID-1]);
    $('.promotion-price').html(promo_price[roomID-1]);
    $('.size').html(size[roomID-1]);
    $('.level').html(level_lang[roomID-1]);
    $('.room-gallery').empty();
    $.each(g,function(index,value){
        $('.room-gallery').append(`
            <li>
                <div class='image' style="background: url('assets/img/picture/`+value+`') center center">
                </div>
            </li>
        `)
    });
    $('.equipment-list').empty();
    $.each(equip_lang,function(index,value){
        $('.equipment-list').append(`
            <li class='row'>
                <div class='col-sm-1'>
                    <i class="fas fa-check-circle check-icon"></i>
                </div>
                <div class='col-sm-11'>
                    `+value+`
                </div>
            </li>
        `)
    });
    $('.room-description-label').html(room_desc_lang[roomID-1]);
    slider.refresh();
}

$(document).ready(function(){

    $(document).on('change','.room-select',function(){
        updateRoomDetail();
    })

    //Menu item function
    $(window).load(function() {
        $('.room-menu').addClass('active');
    });

    //Slider function
    slider = $("#gallery-slider").lightSlider( {
        loop:true, item:1, enableTouch:true, enableDrag:true, centerMode:true, variableWidth:true, responsive:[ {
            breakpoint:1024, settings: {
                    item: 1,
                }
            }
            , {
            breakpoint:768, settings: {
                    item:1,
                }
            }
            , {
            breakpoint:480, settings: {
                    item: 1,
                }
            }
        ]
    });
    updateRoomDetail();
})
