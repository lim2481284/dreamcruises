
/* jQuery Pre loader
-----------------------------------------------*/
$(window).load(function(){
    $('.preloader').fadeOut(1000); // set duration in brackets

    //Check login
    if(localStorage.getItem("login")==1)
    {
        $('.login-btn').hide();
        $('.logout-btn').show();
        $('.booking-btn').show();
    }
    else
    {
        $('.login-btn').show();
        $('.logout-btn').hide();
        $('.booking-btn').hide();
    }

    //Mobile menu toggle
    $('.navbar-toggle').click(function(){
        $(this).toggleClass('active-menu');
    })

    //Book button
    $('.buy-now-btn, .booking-btn').click(function(){
        location.href='booking.html';
    })

});


$(document).ready(function() {

    //Onclick login
    $(document).on('click','.login',function(){
        localStorage.setItem("login", "1");
        location.reload();
    })
    //Onclick login
    $(document).on('click','.logout-btn',function(){
        localStorage.setItem("login", "0");
        location.reload();
    })


    //Load layout
    $(window).load(function() {
        localization();        
     });


    /* Language function
    -----------------------------------------------*/
    $("#selectLanguageDropdown").mouseenter(function(){
        $("#selectLanguageDropdown").click();
    })


    /* Hide mobile menu after clicking on a link
    -----------------------------------------------*/
    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });


    /* Back to Top
    -----------------------------------------------*/
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });
    // Animate the scroll to top
    $('.go-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 300);
    });


    /* wow
    -------------------------------*/
    new WOW({ mobile: false }).init();


    /* Login function
    -------------------------------*/
    $(document).on('click','.login-btn',function(){
        $('.login-form-section').fadeIn('slow');
    });
    $(document).on('click','.login-message a',function(){
        $('.login-form-panel form').animate({
            height: "toggle",
            opacity: "toggle"
        }, "slow");
    });
    $(document).on('click','.close-login-form-btn, .login-form-outside',function(){
        $('.login-form-section').fadeOut('fast');
    })


    /* nav scroll function
    var nav = $("body");
    $(window).scroll(function () {
        var $this = $(this);
        if ($this.scrollTop() > 20) {
          $('.navbar-fixed-top').addClass("nav-fixed");
        } else {
           $('.navbar-fixed-top').removeClass("nav-fixed");
        }
    });
    -------------------------------*/

});
