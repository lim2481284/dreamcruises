$(document).ready(function(){

    //Menu item function
    $(window).load(function() {
        $('.plan-menu').addClass('active');
    });

    //Slider function
    $("#gallery-slider").lightSlider( {
        loop:true, item:4, enableTouch:true, enableDrag:true, centerMode:true, variableWidth:true, responsive:[ {
            breakpoint:1024, settings: {
                    item: 3,
                }
            }
            , {
            breakpoint:768, settings: {
                    item:2,
                }
            }
            , {
            breakpoint:480, settings: {
                    item: 1,
                }
            }
        ]
    });
})
