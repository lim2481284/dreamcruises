function localization(){
    $('#selectLanguageDropdown').localizationTool({
        'onLanguageSelected': function(lang){
            localStorage.setItem("language", lang);
            $('#notificationModal').modal('toggle');
            try {
                updateRoomDetail(1);
            }
            catch(err) {
                return true;
            }

        },
        'defaultLanguage' : '简体中文',
        'showFlag': false,
        'showCountry': false,
        'showLanguage': true,
        'languages' : {
            '简体中文' : {
                'country': '中国',
                'language' : '简体中文',
                'countryTranslated': '中国',
                'languageTranslated': '中文',
                'flag' : {
                }
            },
            'barletta-dialect' : {
                'country': 'Barletta',
                'language' : 'Barlettano',
                'countryTranslated': 'Barlett',
                'languageTranslated': "Barlett'n",
                'flag': {
                    'url' : 'http://upload.wikimedia.org/wikipedia/commons/2/20/Flag_of_Barletta.png'
                }
            }
        },
        /*
        * Translate your strings below
        */
        'strings' : {
            'class:bar-label' : {
                'en_GB' : 'Restaurant and bar'
            },
            'class:click-here-label' : {
                'en_GB' : 'Click Here'
            },
            'class:understand-label' : {
                'en_GB' : 'Okay, got it!'
            },
            'class:agreement2-modal-label' : {
                'en_GB' : `Genting Hong Kong Limited and its subsidiaries (Genting Hong Kong Group) would like to keep in touch with you regarding special offers, promotional material and other news in relation to cruise and cruise related operations, retail products and services*, securities trading and financial services, and leisure, entertainment, hospitality and related services (Services). We need your consent to use your name, address, email address and/or telephone number (Contact Information) for such direct marketing. Further, we would like to transfer your Contact Information to the associated companies of Genting Hong Kong Limited in various countries for direct marketing of their Services and we need your written consent for such transfer. Please tick the box if you consent to such use and transfer.*Our, and our associated companies, retail businesses consist of onboard, in resort, online and other related stores which market a wide variety of products, such as clothing, bags, shoes, beauty products, jewelry, watches, leather goods, electronics, decorations, souvenirs, artworks, kitchen ware, toys, stationery, medication, consumable goods (for example, alcoholic and non-alcoholic beverage, food, health supplements and tobacco, etc.) and high-end consumer goods. Please note that this list is not exhaustive, and the product offerings will change from time to time.`
            },
            'class:agreement3-modal-label' : {
                'en_GB' : `Terms and Conditions for Online / Mobile Booking

Please review the following terms and conditions carefully. By using this site or any part of it, you agree that you have read these Terms and Conditions and that you accept and agree to be bound by them.

These Terms and Conditions apply to all aspects of the online and mobile booking process with Dream Cruises. These Terms and Conditions may be amended at any time without notice. It is your responsibility to check for the updated version each time before you place booking with Dream Cruises.

Dream Cruises shall make every effort to ensure that fares, fees, information of goods and services set out in this site are accurate, complete and up to date. Dream Cruises neither warrants nor makes any representations as to the accuracy, completeness, timeliness, reliability and fitness for a particular purpose of any information set out in this site. Fares set out in this site do not reflect availability of Dream Cruises’ goods and services. All bookings are subject to Dream Cruises’ acceptance at its sole discretion and availability of the goods and services. Information set out in this site is subject to change without prior notice.

Fares / fees charged are those applicable on the day you place the booking.

Any payment to be made for goods or services shall be subject to the applicable exchange rates of the supported currencies. The exchange rates shall be determined at the sole discretion of Dream Cruises from time to time and shall be final and not negotiable.

You must provide Dream Cruises with accurate, complete and up-to-date information for placing your booking.

Passage Contract, Cancellation Policy and Amendment Policy will apply to your booking. Please visit home page of this site for reading those terms and policies before you place booking with Dream Cruises.

By completing a booking on this site, you agree to be bound by these Terms and Conditions, Passage Contract, Cancellation Policy and Amendment Policy.

You will receive a formal confirmation slip by email from Dream Cruises after making payment within the specified time. If you do not receive a confirmation slip within 24 hours after making payment, please call Dream Cruises’ hotline or email Dream Cruises.

Hotline:

Hong Kong : + (852) 2317 7711
Singapore : + (65) 6223 0002 or + (65) 6808 2288
Penang, Malaysia : + (60) 4 2631128
Kuala Lumpur, Malaysia : + (60) 3 2302 1288
China : + (86) 400 8499 848 or (86) 400 8810 348
Others : IDD call
Email Address:

Reservations@dreamcruiseline.com (English)
Reservations.cn@dreamcruiseline.com (Chinese)
Dream Cruises will cancel your booking without prior notice if you fail to make payment within the specified time

Should you wish to cancel and make any amendments to your booking which has been confirmed by Dream Cruises, please call Dream Cruises’ hotline or email to Dream Cruises.

In the event that a good or service that you have placed an order is not available, Dream Cruises shall use its reasonable endeavours to suggest an alternative good or service of similar pricing and quality.

In case the alternative good or service suggested by Dream Cruises is not acceptable to you but you have already made payment for good or service on this site, Dream Cruises shall make a refund. If you have not accepted the suggested alternative by the specified time, you will be deemed to have refused the suggested alternative.

It is the sole responsibility of the passenger to obtain a valid passport and visa for visiting certain destinations. Compulsory inoculations are also the sole responsibility of the passenger. Dream Cruises shall not be liable for any loss or delay if the passenger fails to obtain the relevant travel documents.

Dream Cruises shall not be liable for any loss and damage directly or indirectly, specially or incidentally arising from (a) the use of any part of this site; (b) any failure or delay in providing goods or services through this site.

If you have registered an account for using Online / Mobile Booking, you shall safeguard username and password. Dream Cruises assume that any person using this site with your username and password is either you or is authorized by you.

Dream Cruises has an absolute discretion to refuse registration of an account for using Online / Mobile Booking on this site, and to terminate the registration for any reason whatsoever.

Dream Cruises shall not be liable for any loss or disclosure of your personal details obtained from this site.

Dream Cruises may, at any time terminate or restrict your access to Online / Mobile Booking, or refuse to give effect to any booking you have placed without prior notice in the event that Dream Cruises suspects that Online / Mobile Booking is being used, or may be used : -

in breach of any of these Terms and Conditions;
in breach of any terms of the Passage Contract; or
in a manner that may cause loss to you or Dream Cruises
There shall be no guarantee to the security of online / mobile communication and transaction. You shall be alerted that transaction on made through the internet, online or mobile may be interfered, and the transmission may be interrupted, delayed or the data transmitted may be corrupted. Dream Cruises shall not be liable for any loss and damage directly or indirectly, specially or incidentally arising from Online Booking, Mobile Booking and the use of this site.

Dream Cruises uses third party payment processor(s) for online transactions. Dream Cruises’ payment processor(s) and the payment method(s) accepted by it/them may change from time to time without notice. By using such third party payment processor(s), you agree and accept that your credit card information (including without limitation your name and credit card number) will be collected, processed and retained by such payment processor(s) subject to its/their terms and conditions. You agree that you shall be wholly responsible for any losses and/or damages incurred or sustained by you in making the online transaction with such payment processor(s). In no event shall Dream Cruises be liable for any such losses and/or damages.

For the avoidance of doubt, cruise operator of Dream Cruises’ vessels is Dream Cruises Management Limited.

These Terms and Conditions shall be governed by and construed in accordance with the laws of Hong Kong SAR and you shall submit to the exclusive jurisdiction of the courts of Hong Kong SAR.

Any part of these Terms and Conditions which is prohibited or unenforceable in any jurisdiction is ineffective as to that jurisdiction only to the extent of the prohibition or unenforceability. That does not invalidate the remaining parts of these Terms and Conditions nor affect the validity or enforceability of that part in any other jurisdiction.

These Terms and Conditions have been translated into Chinese. If there is any inconsistency or ambiguity between the English version and the Chinese version, the English version shall prevail.

Last Updated Date : 2017-09-01`
            },
            'class:travel-document-label' : {
                'en_GB' : 'Travel Document Requirement'
            },
            'class:immigration-label' : {
                'en_GB' : 'World Dream - Immigration Requirements'
            },
            'class:notification-title-label' : {
                'en_GB' : 'Passengers\' entry and exit must know'
            },
            'class:wellness-label' : {
                'en_GB' : 'Wellness facilities'
            },
            'class:beauty-label' : {
                'en_GB' : 'Beauty and health'
            },
            'class:cruises-kid-label' : {
                'en_GB' : 'Teenage and children facilities'
            },
            'class:recreation-label' : {
                'en_GB' : 'Entertainment'
            },
            'class:shop-label' : {
                'en_GB' : 'Retail'
            },
            'class:other-label' : {
                'en_GB' : 'Other service'
            },
            'class:club-label' : {
                'en_GB' : 'Genting Club'
            },
            'class:king-label' : {
                'en_GB' : 'Royal palace'
            },
            'class:mouse-hover-label' : {
                'en_GB' : 'Hover over the image / click on the image'
            },
            'class:download-label' : {
                'en_GB' : 'Download Plans Document'
            },
            'class:bar-1-label' : {
                'en_GB' : 'Palm Court'
            },
            'class:bar-2-label' : {
                'en_GB' : 'Sun Deck Bar'
            },
            'class:bar-3-label' : {
                'en_GB' : 'The Lido'
            },
            'class:bar-4-label' : {
                'en_GB' : 'Pool Deck Bar'
            },
            'class:bar-5-label' : {
                'en_GB' : 'Crystal Life Cuisine'
            },
            'class:bar-6-label' : {
                'en_GB' : 'Dream Dining (Room Upper)'
            },
            'class:bar-7-label' : {
                'en_GB' : 'Hot Pot'
            },
            'class:bar-8-label' : {
                'en_GB' : 'Umi Uma'
            },
            'class:bar-9-label' : {
                'en_GB' : 'Prime Steakhouse'
            },
            'class:bar-10-label' : {
                'en_GB' : 'Seafood Grill'
            },
            'class:bar-11-label' : {
                'en_GB' : 'Vintage Room'
            },
            'class:bar-12-label' : {
                'en_GB' : 'Bar City'
            },
            'class:bar-13-label' : {
                'en_GB' : 'Johnnie Walker House'
            },
            'class:bar-14-label' : {
                'en_GB' : 'Penfolds Wine Vault'
            },
            'class:bar-15-label' : {
                'en_GB' : 'Bubles Champagne Bar'
            },
            'class:bar-16-label' : {
                'en_GB' : 'Mixt Cocktail Bar'
            },
            'class:bar-17-label' : {
                'en_GB' : 'Dream Dining Room Lower'
            },
            'class:bar-18-label' : {
                'en_GB' : 'Blue Lagoon'
            },
            'class:bar-19-label' : {
                'en_GB' : 'Red Lion'
            },
            'class:bar-20-label' : {
                'en_GB' : 'Bar 360'
            },
            'class:bar-21-label' : {
                'en_GB' : 'Lobby Cafe'
            },
            'class:bar-22-label' : {
                'en_GB' : 'Silk Road Chinese Restaurant '
            },
            'class:wellness-1-label' : {
                'en_GB' : 'Waterslide Park'
            },
            'class:wellness-2-label' : {
                'en_GB' : 'SportsPlex'
            },
            'class:wellness-3-label' : {
                'en_GB' : 'Mini-Golf'
            },
            'class:wellness-4-label' : {
                'en_GB' : 'Rock Climbing Wall'
            },
            'class:wellness-5-label' : {
                'en_GB' : 'Ropes Course'
            },
            'class:wellness-6-label' : {
                'en_GB' : 'Sun Deck'
            },
            'class:wellness-7-label' : {
                'en_GB' : 'Jogging Track'
            },
            'class:wellness-8-label' : {
                'en_GB' : 'Main Pool Deck'
            },
            'class:wellness-9-label' : {
                'en_GB' : 'Kids Water Park'
            },
            'class:wellness-10-label' : {
                'en_GB' : 'Broadwalk'
            },
            'class:beauty-1-label' : {
                'en_GB' : 'Crystal Life Fitness'
            },
            'class:beauty-2-label' : {
                'en_GB' : 'Crystal Life Spa'
            },
            'class:beauty-3-label' : {
                'en_GB' : 'Le Salon'
            },
            'class:beauty-4-label' : {
                'en_GB' : 'The Gentlemen’s Barber'
            },
            'class:beauty-5-label' : {
                'en_GB' : 'Crystal Life Asian Spa'
            },
            'class:kid-1-label' : {
                'en_GB' : 'Little Pandas Club'
            },
            'class:kid-2-label' : {
                'en_GB' : 'Arcade'
            },
            'class:recreation-1-label' : {
                'en_GB' : 'Zouk Private Party Deck'
            },
            'class:recreation-2-label' : {
                'en_GB' : 'Zouk Beach Club'
            },
            'class:recreation-3-label' : {
                'en_GB' : 'VR EXPERIENCE LAB'
            },
            'class:recreation-4-label' : {
                'en_GB' : 'Resorts World AT SEA'
            },
            'class:recreation-5-label' : {
                'en_GB' : 'Zodiac Theatre'
            },
            'class:recreation-6-label' : {
                'en_GB' : 'Board Games Room'
            },
            'class:recreation-7-label' : {
                'en_GB' : 'OK Karaoke'
            },
            'class:recreation-8-label' : {
                'en_GB' : 'International Room'
            },
            'class:recreation-9-label' : {
                'en_GB' : 'Premium Room'
            },
            'class:shop-1-label' : {
                'en_GB' : 'Art Gallery'
            },
            'class:shop-2-label' : {
                'en_GB' : 'The Dream Boutiques'
            },
            'class:shop-3-label' : {
                'en_GB' : 'The Dream Store'
            },
            'class:shop-4-label' : {
                'en_GB' : 'Souvenir Mart'
            },
            'class:shop-5-label' : {
                'en_GB' : 'Photo Corner'
            },
            'class:other-1-label' : {
                'en_GB' : 'Business Centre'
            },
            'class:other-2-label' : {
                'en_GB' : 'Bridge Viewing Room'
            },
            'class:other-3-label' : {
                'en_GB' : 'Helipad Embarkation Lounge'
            },
            'class:other-4-label' : {
                'en_GB' : 'Function 8 – Multi-purpose Venue'
            },
            'class:other-5-label' : {
                'en_GB' : 'Tributes'
            },
            'class:other-6-label' : {
                'en_GB' : 'Executive Boardrooms'
            },
            'class:other-7-label' : {
                'en_GB' : 'Reception'
            },
            'class:other-8-label' : {
                'en_GB' : 'Shore Excursions & Cruise Consultant'
            },
            'class:other-9-label' : {
                'en_GB' : 'Meeting Rooms'
            },
            'class:other-10-label' : {
                'en_GB' : 'Clinic'
            },
            'class:club-1-label' : {
                'en_GB' : 'Club'
            },
            'class:club-2-label' : {
                'en_GB' : 'Lounge'
            },
            'class:king-1-label' : {
                'en_GB' : 'Palace Pool Side Cafe'
            },
            'class:king-2-label' : {
                'en_GB' : 'Palace pool & Sun Deck'
            },
            'class:king-3-label' : {
                'en_GB' : 'Palace Gymnasium and Studio'
            },
            'class:king-4-label' : {
                'en_GB' : 'Palace Spa'
            },
            'class:king-5-label' : {
                'en_GB' : 'Palace Restaurant'
            },
            'class:king-6-label' : {
                'en_GB' : 'Palace Lounge'
            },
            'class:king-7-label' : {
                'en_GB' : 'Palace Reception'
            },
            'class:plan-title-label' : {
                'en_GB' : '5-Night Okinawa Discovery'
            },
            'class:plan-description-label' : {
                'en_GB' : 'Discover the island gems of southern Japan on board World Dream. Guests can experience the scenic and cultural wonders of the land of the rising sun from the comfort of their own floating, integrated resort at sea.'
            },
            'class:plan-route-label' : {
                'en_GB' : 'Hong Kong - Japan - Hong Kong'
            },
            'class:now-booking-label' : {
                'en_GB' : 'Booking Now'
            },
            'class:route-start-label' : {
                'en_GB' : 'Hong Kong | Guangzhou, China Departure (5 Nights, Sun – Fri)'
            },
            'class:travel-label' : {
                'en_GB' : 'Travel'
            },
            'class:d1-label' : {
                'en_GB' : 'Sunday'
            },
            'class:d2-label' : {
                'en_GB' : 'Monday'
            },
            'class:d3-label' : {
                'en_GB' : 'Tuesday'
            },
            'class:d4-label' : {
                'en_GB' : 'Wednesday'
            },
            'class:d5-label' : {
                'en_GB' : 'Thursday'
            },
            'class:d6-label' : {
                'en_GB' : 'Friday'
            },
            'class:t1-label' : {
                'en_GB' : 'Arrived Hong Kong'
            },
            'class:t2-label' : {
                'en_GB' : 'Cruising Day'
            },
            'class:t3-label' : {
                'en_GB' : 'Arrived Naha'
            },
            'class:t4-label' : {
                'en_GB' : 'Arrived Miyakojima'
            },
            'class:t5-label' : {
                'en_GB' : 'Cruising Day'
            },
            'class:t6-label' : {
                'en_GB' : 'Arrived Hong Kong'
            },
            'class:travel-note-label' : {
                'en_GB' : 'Itineraries are subject to change at any time without notice, please refer to the special announcement for latest arrangement. *This cruise will pick up passengers in Guangzhou after sailing out from Hong Kong, and drop off passengers in Guangzhou before sailing back to Hong Kong. Please note that passengers cruising from Hong Kong are not allowed to disembark in Guangzhou, and likewise, passengers cruising to Hong Kong are not allowed to disembark in Guangzhou.Dream Cruises charges a per person gratuity as part of our cruise offering. The gratuity will be charged onboard to our guests’ stateroom folio at the end of the cruise when the guest settles their onboard expenses. Gratuities charge policy is subject to change at any time. Click here for more information about gratuities.'
            },
            'class:pic-note-label' : {
                'en_GB' : 'Photos are for reference only.'
            },
            'class:original-label' : {
                'en_GB' : 'Original Price'
            },
            'class:promotion-label' : {
                'en_GB' : 'First Week Promotion Price'
            },
            'class:size-label' : {
                'en_GB' : 'Area'
            },
            'class:size2-label' : {
                'en_GB' : 'Square Meters'
            },
            'class:headcount-label' : {
                'en_GB' : 'Capacity'
            },
            'class:level-label' : {
                'en_GB' : 'Room Level'
            },
            'class:equipment-label' : {
                'en_GB' : 'Room facilities'
            },
            'class:room1-label' : {
                'en_GB' : 'Palace Deluxe Suite'
            },
            'class:room2-label' : {
                'en_GB' : 'Palace Suite'
            },
            'class:room3-label' : {
                'en_GB' : 'Balcony Deluxe Stateroom'
            },
            'class:room4-label' : {
                'en_GB' : 'Balcony Stateroom'
            },
            'class:room5-label' : {
                'en_GB' : 'Oceanview Stateroom'
            },
            'class:room6-label' : {
                'en_GB' : 'Interior Stateroom'
            },


            'class:choose-room-label' : {
                'en_GB' : 'Select Room'
            },
            'class:double-confirm-label' : {
                'en_GB' : 'Review Booking'
            },
            'class:personal-label' : {
                'en_GB' : 'Guest Information'
            },
            'class:payment-label' : {
                'en_GB' : 'Payment Information'
            },
            'class:book-success-label' : {
                'en_GB' : 'Booking Complete'
            },
            'class:please-choose-room-label' : {
                'en_GB' : 'Please choose a room'
            },
            'class:click-to-add-room-label' : {
                'en_GB' : 'Please click on Add to Cart button to add to your reservation'
            },
            'class:room1-price-label' : {
                'en_GB' : 'Palace Deluxe Suite CNY10488 '
            },
            'class:room2-price-label' : {
                'en_GB' : 'Palace Suite CNY9699'
            },
            'class:room3-price-label' : {
                'en_GB' : 'Balcony Deluxe Stateroom CNY7222'
            },
            'class:room4-price-label' : {
                'en_GB' : 'Balcony Stateroom CNY6888'
            },
            'class:room5-price-label' : {
                'en_GB' : 'Oceanview Stateroom CNY6333 '
            },
            'class:room6-price-label' : {
                'en_GB' : 'Interior Stateroom CNY5699 '
            },
            'class:adult-label' : {
                'en_GB' : 'Number of adults (Age 12 or Above)'
            },
            'class:teen-label' : {
                'en_GB' : 'Number of child (Under 12)'
            },
            'class:kid-label' : {
                'en_GB' : 'Number of infant (Under 2) '
            },
            'class:add-room-label' : {
                'en_GB' : 'Add to cart'
            },
            'class:double-confirm-note-label' : {
                'en_GB' : 'Please Review Booking and Cruises Plan'
            },
            'class:route-details-label' : {
                'en_GB' : 'Cruises Plan Details'
            },
            'class:booking-details-label' : {
                'en_GB' : 'Booking Details'
            },
            'class:cruises-only-label' : {
                'en_GB' : 'Cruises'
            },
            'class:routeplan-label' : {
                'en_GB' : 'Schedule'
            },
            'class:depart-label' : {
                'en_GB' : 'Departure'
            },
            'class:depart-date-label' : {
                'en_GB' : '25 Nov 2018'
            },
            'class:port-label' : {
                'en_GB' : 'Port'
            },
            'class:depart-port-location-label' : {
                'en_GB' : 'Hong Kong'
            },
            'class:arrived-label' : {
                'en_GB' : 'Arrival'
            },
            'class:arrived-date-label' : {
                'en_GB' : "30 Nov 2018"
            },
            'class:date-label' : {
                'en_GB' : "Date"
            },
            'class:time-label' : {
                'en_GB' : "Time"
            },
            'class:arrived-port-location-label' : {
                'en_GB' : 'Hong Kong'
            },
            'class:personal-label' : {
                'en_GB' : 'Guest Information'
            },
            'class:fill-in-all-label' : {
                'en_GB' : 'Please fill in all the input'
            },
            'class:self-message-label' : {
                'en_GB' : 'Personal'
            },
            'class:full-name-with-ic-label' : {
                'en_GB' : 'Full Name  (Exactly as shown in your passport or personal identification document)'
            },
            'class:birth-label' : {
                'en_GB' : 'Date of Birth'
            },
            'class:stay-label' : {
                'en_GB' : 'Place Of Residence'
            },
            'class:phone-label' : {
                'en_GB' : 'Mobile Number'
            },
            'class:booking-your-plan-label' : {
                'en_GB' : 'Book Your Dream Holiday '
            },
            'class:email-label' : {
                'en_GB' : 'Email address'
            },
            'class:travel-required-label' : {
                'en_GB' : 'Travel Document Details'
            },
            'class:passport-label' : {
                'en_GB' : 'Document'
            },
            'class:passport-num-label' : {
                'en_GB' : 'Document Number'
            },
            'class:passport-valid-label' : {
                'en_GB' : 'Document Date Expiry'
            },
            'class:passport-location-label' : {
                'en_GB' : 'Place of Issuance'
            },
            'class:eme-info-label' : {
                'en_GB' : 'Emergency Contact Information'
            },
            'class:full-name-label' : {
                'en_GB' : 'Full name'
            },
            'class:agree-1-label' : {
                'en_GB' : 'I agree to join Dream Cruises Loyalty Programme. I confirm that I am aged 18 or above. I declare that I have read, understood and agreed to Dream Cruises Loyalty Programme’s Terms And Conditions'
            },
            'class:agree-2-label' : {
                'en_GB' : 'I agree to the proposed use/transfer of my personal data for direct marketing. Learn More'
            },
            'class:payment-info-label' : {
                'en_GB' : 'Payment Information'
            },
            'class:fill-in-payment-label' : {
                'en_GB' : 'Please Enter Payment Information. '
            },
            'class:total-amount-label' : {
                'en_GB' : 'Grand Total '
            },
            'class:bank-card-label' : {
                    'en_GB' : 'Credit card/Debit card'
            },
            'class:bank-card-type-label' : {
                'en_GB' : 'Credit Type'
            },
            'class:bank-card-num-label' : {
                'en_GB' : 'Card Number'
            },
            'class:bank-card-valid-label' : {
                'en_GB' : 'Expiration Date'
            },
            'class:safe-num-label' : {
                'en_GB' : 'CVV'
            },
            'class:bill-address-label' : {
                'en_GB' : 'Billing Address'
            },
            'class:first-name-label' : {
                'en_GB' : 'First Name'
            },
            'class:last-name-label' : {
                'en_GB' : 'Last Name'
            },
            'class:country-label' : {
                'en_GB' : 'Country'
            },
            'class:city-label' : {
                'en_GB' : 'City'
            },
            'class:country-code-label' : {
                'en_GB' : 'Postal Code'
            },
            'class:agree3-label' : {
                'en_GB' : 'I have read and accepted the terms and conditions.'
            },
            'class:previous-btn-label' : {
                'en_GB' : 'Previous'
            },'class:next-btn-label' : {
                'en_GB' : 'Next'
            },
            'class:booking-success-label' : {
                'en_GB' : 'Booking Success'
            },
            'class:enjoy-holiday-label' : {
                'en_GB' : 'Enjoy Your Dream Holiday!'
            },
            'class:complete-booking-label' : {
                'en_GB' : 'Complete!'
            },
            'class:deck-plan-label' : {
                'en_GB' : 'Deck Plans'
            },
            'class:interior-label' : {
                'en_GB' : 'Interior Stateroom'
            },
            'class:oceanview-label' : {
                'en_GB' : 'Oceanview Stateroom'
            },
            'class:balcony-deluxe-label' : {
                'en_GB' : 'Balcony Deluxe Stateroom'
            },
            'class:balcony-label' : {
                'en_GB' : 'Balcony Stateroom'
            },
            'class:palace-deluxe-label' : {
                'en_GB' : 'Palace Deluxe Suite'
            },
            'class:palace-label' : {
                'en_GB' : 'Palace Suite'
            },
            'class:logout-label' : {
                'en_GB' : 'Logout'
            },
            'class:booking-label' : {
                'en_GB' : 'Booking'
            },
            'class:footer-email-label' : {
                'en_GB' : 'Email Reservation:  reservations.cn@gentingcruiselines.com'
            },
            'class:hotline-label' : {
                'en_GB' : 'Reservation Hotline: +86-4008499848'
            },
            'class:guest-room-label' : {
                'en_GB' : 'Guest Room'
            },
            'class:route-date-label' : {
                'en_GB' : 'November 25 - November 30'
            },
            'class:japan-label' : {
                'en_GB' : 'Japan'
            },
            'class:hong-kong-label' : {
                'en_GB' : 'Hong Kong'
            },
            'class:route-label' : {
                'en_GB' : 'Cruise Route'
            },
            'class:know-more-label' : {
                'en_GB' : 'Find Out More'
            },
            'class:dream-cruises-explain-label' : {
                'en_GB' : 'On World Dream guests can explore the world’s finest on one ship, with an exciting array of Asian and international dining, iconic global brand experiences and diverse entertainment options – all whilst visiting some of Asia’s most inspiring destinations.'
            },
            'class:dream-cruises-label' : {
                'en_GB' : 'Dream Cruises'
            },
            'class:room-label' : {
                'en_GB' : 'Room Intro'
            },
            'class:plan-label' : {
                'en_GB' : 'Cruises Plan'
            },
            'class:cruises-label' : {
                'en_GB' : 'Cruises Intro'
            },
            'class:banner-title-label' : {
                'en_GB' : 'Dream Cruises'
            },
            'class:banner-subtitle-label' : {
                'en_GB' : 'Start Your Dream Holiday'
            },
            'class:book-now-label' : {
                'en_GB' : 'Book Now'
            },
            'class:username-label' : {
                'en_GB' : 'Username'
            },
            'class:password-label' : {
                'en_GB' : 'Password'
            },
            'class:confirm-password-label' : {
                'en_GB' : 'Confirm Password'
            },
            'class:email-label' : {
                'en_GB' : 'Email'
            },
            'class:create-account-label' : {
                'en_GB' : 'Create Account'
            },
            'class:create-account-now-label' : {
                'en_GB' : 'Create Account Now'
            }
            ,
            'class:already-have-account-label' : {
                'en_GB' : 'Already have an account ?'
            },
            'class:login-now-label' : {
                'en_GB' : 'Login Now'
            },
            'class:no-account-label' : {
                'en_GB' : 'No account ?'
            },
            'class:home-label' : {
                'en_GB' : 'Home'
            },
            'class:login-label' : {
                'en_GB' : 'Login'
            },
            'class:company-label' : {
                'en_GB' : 'Action Club'
            },
            'class:copyright-label' : {
                'en_GB' : 'Copyright'
            }
        }
    });
    var lang = localStorage.getItem("language");
    if(!lang) lang = '简体中文';
    $("#selectLanguageDropdown").localizationTool('translate',lang);
}
